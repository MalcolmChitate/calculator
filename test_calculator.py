import unittest
from calculator import parse_parentheses_in_input, evaluate_input
from exceptions import InconsistentParenthesesException

class TestParseParenthesesInInput(unittest.TestCase):
    def test_input_not_string(self):
        self.assertRaises(TypeError, parse_parentheses_in_input, args = '6*4')

    def test_input_without_closing_parenthesis(self):
        self.assertRaises(InconsistentParenthesesException, parse_parentheses_in_input, '4*7(7*6')

class TestEvaluateInput(unittest.TestCase):
    def test_inconsistent_parentheses_in_input(self):
        self.assertEqual(evaluate_input('4*7(7*6+(5/7)'), "The number of open parentheses doesn't match the closed")

    def test_accuracy_of_decimal_places(self):
        self.assertAlmostEqual(float(evaluate_input('4*77.688707*6+5.456967567/7.897898')), 1865.21990723)

    def test_unsupported_characters(self):
        self.assertIn('Unsupported Character', evaluate_input('430$'))

    def test_negative_operands(self):
        self.assertEqual(float(evaluate_input('-4+4')), 0)

    def test_positive_operands(self):
        self.assertEqual(float(evaluate_input('+4-4*8-3')), -31)

    def test_single_value_input(self):
        self.assertEqual(float(evaluate_input('4')), 4)


if __name__ == "__main__":
    unittest.main()
