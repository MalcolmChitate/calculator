from datetime import datetime
from pytz import timezone
import calculator_db
from pip._vendor.distlib.compat import raw_input
from exceptions import InconsistentParenthesesException


def parse_parentheses_in_input(input = ''):
    stack = []
    parentheses_expression = []
    open_parentheses_counter = 0
    closed_parentheses_counter = 0
    for char_index, char in enumerate(input):
        if char == '(':
            stack.append(char_index)
            open_parentheses_counter+=1
        elif char == ')' and stack:
            start = stack.pop()
            closed_parentheses_counter+=1
            parentheses_expression.append((len(stack), input[start + 1: char_index]))
    if open_parentheses_counter == closed_parentheses_counter:
        return parentheses_expression
    else:
        raise InconsistentParenthesesException("The number of open parentheses doesn't match the closed")


def evaluate_input(input=""):
    input.replace(" ", "")
    supported_characters = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "+", "-", "*", "/", ".", "(", ")"]
    for character in input:
        if character not in supported_characters:
            return "Unsupported Character: " + character
    try:
        parentheses_expressions = list(parse_parentheses_in_input(input))
    except InconsistentParenthesesException as e:
        return str(e)
    if parentheses_expressions:
        input_copy = input
        parentheses_expression_hold = None
        evaluated_expression = None
        for level, parentheses_expression in parentheses_expressions:
            outer_open_bracket_index = parentheses_expression.find('(')
            outer_close_bracket_index = -1
            for char_index, char in enumerate(parentheses_expression):
                if ')' == char:
                   outer_close_bracket_index = char_index 
            if evaluated_expression != None and outer_open_bracket_index!=-1 and outer_close_bracket_index != -1:
                parentheses_expression = parentheses_expression[ :outer_open_bracket_index] + evaluated_expression + parentheses_expression[outer_close_bracket_index + 1: ] 
            parentheses_expression_hold = parentheses_expression
            evaluated_expression = eval(parentheses_expression)
            evaluated_input = input_copy.replace('(' + parentheses_expression + ')', evaluated_expression)
            input_copy = evaluated_input
        return eval(input_copy)
    else:
        return eval(input)


dbms = None
# Program entry point
def main():
    dbms = calculator_db.CalculatorDb(calculator_db.SQLITE, dbname='calculatordb.sqlite')
    # Create Tables
    dbms.create_db_tables()

    input_string = raw_input("Enter your expression after the colon : ")
    input_string = input_string.replace(" ", "")

    def get_output(input=''):
        result = evaluate_input(input)
        query = "INSERT INTO History(expression, result, created_on) VALUES (:expression, :result, :created_on)"
        data = {"expression": input, "result": result, "created_on": datetime.now(timezone('Africa/Harare'))}
        dbms.execute_query(query, data)
        with dbms.db_engine.connect() as connection:
            try:
                connection.execute()
            except Exception as e:
                pass
                # print(e)
        return result

    print('Answer: ' + str(get_output(input_string)))
    dbms.print_all_data(table="history")


# run the program
if __name__ == "__main__": main()

