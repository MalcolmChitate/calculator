from sqlalchemy import create_engine
from sqlalchemy import Table, Column, Integer, String, MetaData, ForeignKey, Float, text, DateTime
import datetime
# Global Variables
SQLITE                  = 'sqlite'

# Table Names
HISTORY          = 'history'

class CalculatorDb:
    # http://docs.sqlalchemy.org/en/latest/core/engines.html
    DB_ENGINE = {
        SQLITE: 'sqlite:///{DB}'
    }

    # Main DB Connection Ref Obj
    db_engine = None
    def __init__(self, dbtype, username='', password='', dbname=''):
        dbtype = dbtype.lower()
        if dbtype in self.DB_ENGINE.keys():
            engine_url = self.DB_ENGINE[dbtype].format(DB=dbname)
            self.db_engine = create_engine(engine_url)
        else:
            print("DBType is not found in DB_ENGINE")

    def create_db_tables(self):
        metadata = MetaData()
        history = Table(HISTORY, metadata,
                      Column('id', Integer, primary_key=True),
                      Column('expression', String),
                      Column('result', Float),
                      Column('created_on', DateTime, default=datetime.datetime.now())
                      )
        try:
            metadata.create_all(self.db_engine)
        except Exception as e:
            print("Error occurred during Table creation!")
            print(e)

        # Insert, Update, Delete
    def execute_query(self, query='', data={}):
        if query == '': return
        statement = text(query)
        with self.db_engine.connect() as connection:
            try:
                connection.execute(statement, data)
            except Exception as e:
                print(e)


    def print_all_data(self, table='', query=''):
        query = query if query != '' else "SELECT * FROM '{}' ORDER BY ID DESC;".format(table)
        print("\nYour Recent History")
        with self.db_engine.connect() as connection:
            try:
                result = connection.execute(query)
            except Exception as e:
                print(e)
            else:
                for row in result:
                    print("{}:   {} = {}".format(row[3], row[1], row[2]))
                result.close()
        print("\n")