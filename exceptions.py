class InconsistentParenthesesException(Exception):
    """Raised when the number of open parentheses doesn't match the closed"""
    pass